public class BaseElement {

    public String locator = "//*[id='some-id']";

    public void click() {
        System.out.println("Clicked on element");
    }

    public void getElement() {
        System.out.println("Got the element");
    }

}
